// Libraries includes
#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#include "HMC5883L.h"
#include "OneWire.h"
#include <DallasTemperature.h>
#include <MS5611.h>
#include <SPI.h>
#include <SD.h>

#define DEBUG
#define SD_CARD

// pin definitions
#define ONE_WIRE_BUS_TEMP 3

// Instance definition for sensors
MPU6050 accGyroSensor;
HMC5883L magSensor;
MS5611 barSensor;

OneWire oneWire(ONE_WIRE_BUS_TEMP); 
DallasTemperature tempSensor(&oneWire);

// global variables
int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t mx, my, mz;
double temp1;

int iteration = 0;
int batch = 0;

void setup() {

  
  pinMode(A3, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A1, OUTPUT);
  Serial.begin(38400);
  while (!Serial) {
#ifdef DEBUG
    Serial.println("Connecting to serial");
    delay(1000);
#endif
  }

  tempSensor.begin();
  
#ifdef DEBUG
  Serial.println("Initializing Barometer");
#endif

  while(!barSensor.begin()) {
#ifdef DEBUG
    Serial.println("Could not find a valid MS5611 sensor, check wiring!");
#endif
    delay(500);
  }
  
  Serial.print("Initializing SD card...");
#ifdef SD_CARD
  if (!SD.begin(10)) {
    Serial.println("init failed");
  } else {
    String name = batch + "data.csv";
    File dataFile = SD.open("data.csv", FILE_WRITE);
    if (dataFile) {
      dataFile.println("Iteration, Mag_X, Mag_Y, Mag_Z, Acc_X, Acc_Y, Acc_Z, Gyro_X, Gyro_Y, Gyro_Z, Elevation, Temp1, SolarB, SolarA, PhotoR_1, PhotoR_2, PhotoR_3, PhotoR_4, PhotoR_5, PhotoR_6");
      dataFile.close();
    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.txt");
    }
  }
  
#endif
 
  Serial.print("Oversampling: ");
  Serial.println(barSensor.getOversampling());
  
   Wire.begin();
   accGyroSensor.setI2CMasterModeEnabled(false);
   accGyroSensor.setI2CBypassEnabled(true) ;
   accGyroSensor.setSleepEnabled(false);  
#ifdef DEBUG  
   Serial.println("Initializing I2C devices...");
#endif
   accGyroSensor.initialize();
   magSensor.initialize();
   /*
#ifdef DEBUG  
   Serial.println(magSensor.testConnection() ? "HMC5883L connection successful" : "HMC5883L connection failed");
   Serial.println(accGyroSensor.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
#endif 
*/

}

int getValueFromMultiplexor(byte pin) {
  digitalWrite(A3, pin & 1 ? HIGH : LOW);
  digitalWrite(A2, pin & 2 ? HIGH : LOW);
  digitalWrite(A1, pin & 4 ? HIGH : LOW);
  delay(10);
  /*Serial.println(analogRead(A7)); 
  Serial.println(analogRead(A3));
  Serial.println(analogRead(A2));
  Serial.println(analogRead(A1));*/
  return analogRead(A7);  
}
void loop() {

  String dataString = "";
  
  ++iteration;
  if(iteration == 1000) {
    ++batch;
  }

  dataString += iteration;
  dataString += ",";  
   
  accGyroSensor.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  magSensor.getHeading(&mx, &my, &mz);

  dataString += mx;
  dataString += ",";
  dataString += my;
  dataString += ",";
  dataString += mz;
  dataString += ",";
  dataString += ax;
  dataString += ",";
  dataString += ay;
  dataString += ",";
  dataString += az;
  dataString += ",";
  dataString += gx;
  dataString += ",";
  dataString += gy;
  dataString += ",";
  dataString += gz;
  dataString += ",";

#ifdef DEBUG
  
  Serial.println("mag x y z. acc x y z. gyro x y z");
  Serial.println(dataString);

   
// To calculate heading in degrees. 0 degree indicates North
   float heading = atan2(my, mx);
   if(heading < 0)
     heading += 2 * M_PI;
   Serial.print("heading:\t");
   Serial.println(heading * 180/M_PI);

#endif

// Read altitude
  /*uint32_t rawTemp = barSensor.readRawTemperature();
  uint32_t rawPressure = barSensor.readRawPressure();
  double realTemperature = barSensor.readTemperature()*/
  long realPressure = barSensor.readPressure();
  float absoluteAltitude = barSensor.getAltitude(realPressure);
  dataString += absoluteAltitude;
  dataString += ",";

#ifdef DEBUG
  Serial.println("--");
/*
  Serial.print(" rawTemp = ");
  Serial.print(rawTemp);
  Serial.print(", realTemp = ");
  Serial.print(realTemperature);
  Serial.println(" *C");

  Serial.print(" rawPressure = ");
  Serial.print(rawPressure);*/
  Serial.print(", realPressure = ");
  Serial.print(realPressure);
  Serial.println(" Pa");

  Serial.print(" absoluteAltitude = ");
  Serial.println(absoluteAltitude);
#endif

  tempSensor.requestTemperatures(); // Send the command to get temperature readings
  temp1 = tempSensor.getTempCByIndex(0);
  dataString += temp1;
  
#ifdef DEBUG 
  Serial.print("Temperature 1: ");
  Serial.println(temp1);
#endif

for (int i = 0; i < 8; ++i) {
  int result = getValueFromMultiplexor(i);
#ifdef DEBUG
    Serial.print(" Analog ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(result);
#endif
  dataString += ",";
  dataString += result;
}

#ifdef SD_CARD
  String name = batch + "data.csv";
  File dataFile = SD.open("data.csv", FILE_WRITE);
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening csv.txt");
  }
#endif

#ifdef DEBUG 
  Serial.println("------------------------------------------------------------");
#endif
  delay(1000);
}
